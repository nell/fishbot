#!/usr/bin/python
"""How many wangernumbs have you numberwanged?"""
from fishapi import get_counter, getnick


def bang(pipein, arguments, event):
    wangernumb_key = "wangernumb-{}".format(getnick(event.source))
    wangernumb = get_counter(wangernumb_key)
    return ("You've wanged %d numbers." % wangernumb.count, None)
