#!/usr/bin/python
"""Cause wangernumb your numberwangs."""
import random
import re

from fishapi import get_counter, getnick
from backend import get_session

sql_session = get_session()

def round(string):
    return int(float(string))

def numberwang(self, event):
    numbers = random.sample(range(100), 5)
    foundnumbers = map(round, re.findall("\d*\.\d+|\d+", event.arguments))
    if len(set(numbers) & set(foundnumbers)):
        wangernumb_key = "wangernumb-{}".format(getnick(event.source))
        wangernumb = get_counter(wangernumb_key, sql_session)
        wangernumb.count += 1
        sql_session.commit()
        responses = (
            "That's Numberwang.",
            "That's Wangernumb.",
            "That's Numberwang! Time to rotate the board!",
        )
        event.server.say(self.respond_to(event.source, event.target), random.choice(responses))


expression = ("", numberwang)
